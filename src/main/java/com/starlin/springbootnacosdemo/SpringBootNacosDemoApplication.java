package com.starlin.springbootnacosdemo;

import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySources;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;


/**
 * Created by starlin
 * on 2021/9/06 09:57.
 */
@SpringBootApplication
@NacosPropertySources(value = {
        //@NacosPropertySource(dataId = "spring-boot-nacos-demo", autoRefreshed = true),
        //@NacosPropertySource(dataId = "druid_config", autoRefreshed = true),
        @NacosPropertySource(dataId = "message", autoRefreshed = true),
        @NacosPropertySource(dataId = "message_zh_CN", autoRefreshed = true),
        @NacosPropertySource(dataId = "message_en_US", autoRefreshed = true)
})

public class SpringBootNacosDemoApplication {

    public static void main(String[] args) {
        SpringApplication.run(SpringBootNacosDemoApplication.class, args);
    }

}
