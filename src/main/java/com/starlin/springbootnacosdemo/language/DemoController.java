package com.starlin.springbootnacosdemo.language;

import com.alibaba.nacos.api.config.annotation.NacosConfigListener;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RestController;

/**
 * Created by starlin
 * on 2021/9/06 11:35.
 */
@RestController
@NacosPropertySource(dataId = "message",autoRefreshed = true)
public class DemoController {

    @Autowired
    private PropertiesTools propertiesTools;

    /**
     * 获取国际化配置
     * @param name 配置名称
     * @return String
     */
    @PostMapping(value = "/getProperties")
    public String getProperties (String name) {
        return propertiesTools.getProperties(name);
    }

    @NacosConfigListener(dataId = "message")
    public void onMessage(String test){
        System.out.println(test);
    }
}