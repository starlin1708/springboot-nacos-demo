package com.starlin.springbootnacosdemo.language;

import com.alibaba.nacos.api.config.annotation.NacosConfigListener;
import com.alibaba.nacos.spring.context.annotation.config.NacosPropertySource;
import org.springframework.stereotype.Component;

/**
 * Created by starlin
 * on 2021/9/08 09:57.
 */
@Component
@NacosPropertySource(dataId = "message", autoRefreshed = true)
public class NacosListener {

  @NacosConfigListener(dataId = "message")
  public void onMessage(String test){
      System.out.println(test);
  }
}