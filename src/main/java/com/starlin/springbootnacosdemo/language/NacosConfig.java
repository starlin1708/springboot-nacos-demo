package com.starlin.springbootnacosdemo.language;

import com.alibaba.nacos.api.NacosFactory;
import com.alibaba.nacos.api.PropertyKeyConst;
import com.alibaba.nacos.api.annotation.NacosInjected;
import com.alibaba.nacos.api.config.ConfigService;
import com.alibaba.nacos.api.config.listener.Listener;
import com.alibaba.nacos.api.exception.NacosException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.io.FileUtils;
import org.apache.commons.lang3.StringUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.stereotype.Component;

import java.io.File;
import java.io.IOException;
import java.util.Locale;
import java.util.Properties;
import java.util.concurrent.Executor;

/**
 * Created by starlin
 * on 2021/9/07 15:26.
 */
@Slf4j
@Component
public class NacosConfig {
    /**
     * 应用名称
     */
    @Value("${spring.application.name}")
    private String applicationName;
    /**
     * 命名空间
     */
    private String dNamespace;
    /**
     * 服务器地址
     */
    private String serverAddr;

    @Autowired
    private MessageConfig messageConfig;

    @Autowired
    private ConfigurableApplicationContext applicationContext;

    private static final String DEFAULT_GROUP = "DEFAULT_GROUP";

    private static final String DEFAULT_NAMESPACE = "ffdf9cc5-55e8-4e48-b50b-4fca8db35e67";

    @NacosInjected
    private ConfigService configService;

    @Autowired
    public void init() {
        serverAddr = applicationContext.getEnvironment().getProperty("nacos.config.server-addr");
        dNamespace = applicationContext.getEnvironment().getProperty("nacos.config.dNamespace");
        if (StringUtils.isEmpty(dNamespace)) {
            dNamespace = DEFAULT_NAMESPACE;
        }
        initTip(null);
        initTip(Locale.CHINA);
        initTip(Locale.US);
        log.info("init success,application name :{},Nacos address:{},namespace:{}", applicationName, serverAddr, dNamespace);
    }

    private void initTip(Locale locale) {
        String content = null;
        String dataId = null;
        try {
            if (locale == null) {
                dataId = messageConfig.getBasename() ;
            } else {
                dataId = messageConfig.getBasename() + "_" + locale.getLanguage() + "_" + locale.getCountry() ;
            }
            Properties properties = new Properties();
            properties.put(PropertyKeyConst.SERVER_ADDR, serverAddr);
            properties.put(PropertyKeyConst.NAMESPACE, dNamespace);
            configService = NacosFactory.createConfigService(properties);
            content = configService.getConfig(dataId, DEFAULT_GROUP, 5000);
            if (StringUtils.isEmpty(content)) {
                log.warn("config is null,init fail,dataId:{}", dataId);
                return;
            }
            log.info("init internationalized configuration,config:{}", content);
            saveAsFileWriter(dataId, content);
            setListener(configService, dataId, locale);
        } catch (Exception e) {
            log.error("initializing internationalization configuration fail,error info:{}", e);
        }
    }

    private void setListener(ConfigService configService, String dataId, Locale locale) throws NacosException {
        configService.addListener(dataId, DEFAULT_GROUP, new Listener() {
            @Override
            public void receiveConfigInfo(String configInfo) {
                log.info("Received new internationalization configuration,config:{}", configInfo);
                try {
                    String content = configService.getConfig(dataId, DEFAULT_GROUP, 5000);
                    saveAsFileWriter(dataId, content);
                } catch (Exception e) {
                    log.error("Monitoring internationalization configuration exception，error info :{}", e);
                }
            }

            @Override
            public Executor getExecutor() {
                return null;
            }
        });
    }

    private void saveAsFileWriter(String fileName, String content) {
        String path = System.getProperty("user.dir") + File.separator + messageConfig.getBaseFolder();
        try {
            fileName = path + File.separator + fileName +".properties";
            File file = new File(fileName);
            FileUtils.writeStringToFile(file, content);
            log.info("Internationalization configuration updated! Local file path:{}", fileName);
        } catch (IOException e) {
            log.error("initializing internationalization configuration exception,local file path:{},error info:{}", fileName, e);
        }
    }

}