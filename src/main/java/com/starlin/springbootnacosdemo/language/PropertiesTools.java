package com.starlin.springbootnacosdemo.language;

import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.context.MessageSource;
import org.springframework.context.NoSuchMessageException;
import org.springframework.context.i18n.LocaleContextHolder;
import org.springframework.stereotype.Component;
import java.util.Locale;


@Slf4j
@Component
public class PropertiesTools {

    @Qualifier(value = "myMessageSource")
    @Autowired
    private MessageSource messageSource;

    public String getProperties(String name) {
        try {
            Locale locale = LocaleContextHolder.getLocale();
            return messageSource.getMessage(name, null, locale);
        } catch (NoSuchMessageException e) {
            log.error("get configuration exception,errof info :{}", e);
        }
        return null;
    }

}