//package io.github.syske.springbootnacosdemo;
//
//import com.alibaba.druid.pool.DruidDataSource;
//import org.junit.jupiter.api.Test;
//import org.springframework.beans.factory.annotation.Autowired;
//import org.springframework.boot.test.context.SpringBootTest;
//
//import javax.sql.DataSource;
//import java.sql.Connection;
//import java.sql.SQLException;
//
//@SpringBootTest
//class SpringBootNacosDemoApplicationTests {
//    @Autowired
//    private DataSource dataSource;
//
//    @Test
//    void contextLoads() throws SQLException {
//        //默认数据源
//        System.out.println("默认数据源" + dataSource.getClass());
//        //获得连接
//        Connection connection = dataSource.getConnection();
//        DruidDataSource druidDataSource = (DruidDataSource) dataSource;
//        System.out.println("druid 数据源最大连接数：" + druidDataSource.getMaxActive());
//        System.out.println("druid 数据源初始化连接数：" + druidDataSource.getInitialSize());
//        System.out.println("druid 数据源初始化maxWait：" + druidDataSource.getMaxWait());
//        System.out.println("druid 数据源初始化minIdle：" + druidDataSource.getMinIdle());
//        connection.close();
//    }
//
//}
